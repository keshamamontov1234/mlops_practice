import streamlit as st
from pathlib import Path
import subprocess
import sys
import moviepy.editor as moviepy
import uuid
import os

# Page config and title
st.set_page_config(layout="wide")
st.title("Model Demonstration App")

# Create a temporary directory within the project directory with a short name
temp_dir = Path("temp")
temp_dir.mkdir(parents=True, exist_ok=True)

# Initialize session state variables if they don't exist
if 'video_path' not in st.session_state:
    st.session_state['video_path'] = ""

if 'model' not in st.session_state:
    st.session_state['model'] = None

# Upload file section
uploaded_file = st.file_uploader(
    "Upload a video...", type=["mp4", "mov", "avi", "asf", "m4v"]
)

# Prepare the model
if not st.session_state['model']:
    with st.spinner('Model preparation...'):
        model_path = "models/model_weights/best_small.pt"
        load_model_script = Path('src/models/load_model.py')
        if load_model_script.exists():
            result = subprocess.run(
                [sys.executable, str(load_model_script), model_path],
                capture_output=True, text=True, encoding='utf-8'
            )
            if result.returncode != 0:
                st.error(f"Error loading model: {result.stderr}")
            else:
                st.session_state['model'] = model_path
        else:
            st.error(f"Script {load_model_script} not found!")

# Upload the file and send it to the model
if uploaded_file is not None and not st.session_state['video_path']:
    with st.spinner('Extracting...'):
        short_uuid = uuid.uuid4().hex[:8]  # Short UUID
        unique_output_dir = temp_dir / f"{uploaded_file.name.split('.')[0]}_{short_uuid}"
        unique_output_dir.mkdir(parents=True, exist_ok=True)
        
        video_path = unique_output_dir / uploaded_file.name
        with open(video_path, "wb") as f:
            f.write(uploaded_file.getbuffer())

        preprocess_script = Path('src/data/video_preprocessing.py')
        if preprocess_script.exists():
            result = subprocess.run(
                [sys.executable, str(preprocess_script), str(video_path),
                 str(unique_output_dir)],
                capture_output=True, text=True, encoding='utf-8'
            )
            if result.returncode != 0:
                st.error(f"Error in video preprocessing: {result.stderr}")
            else:
                # Process the video
                process_result = subprocess.run(
                    [sys.executable, 'src/models/process_video.py',
                     st.session_state['model'], str(video_path),
                     str(unique_output_dir), '--conf', '0.3'],
                    capture_output=True, text=True, encoding='utf-8'
                )
                if process_result.returncode != 0:
                    st.error(f"Error in processing: {process_result.stderr}")
                else:
                    intermediate_output = unique_output_dir / "processed_video" / f"{uploaded_file.name.split('.')[0]}.avi"
                    final_output_path = unique_output_dir / "processed_video.mp4"

                    if final_output_path.exists():
                        st.session_state["video_path"] = str(final_output_path)
                        st.success('Successfully processed the video!')
                    else:
                        st.error(f"File {final_output_path} not found!")
                        try:
                            parent_path = Path(process_result.stdout.strip()).parent
                            if len(str(parent_path)) < 255:
                                st.write(f"Contents of directory {parent_path}: {os.listdir(parent_path)}")
                            else:
                                st.warning(f"Path too long to list directory contents: {parent_path}")
                        except Exception as e:
                            st.error(f"Error listing directory contents: {e}")
        else:
            st.error(f"Script {preprocess_script} not found!")

# Check if the video has been processed
if st.session_state['video_path']:
    st.header("Video")
    st.video(st.session_state['video_path'])
