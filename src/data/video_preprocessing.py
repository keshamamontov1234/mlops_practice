import click
from pathlib import Path
import moviepy.editor as moviepy
import os


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_dir", type=click.Path())
@click.option("--fps", default=10, help="FPS для выходного видео.")
def upload_and_preprocess_video(input_filepath, output_dir, fps):
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Сохраняем временный файл в output_dir напрямую
    temp_path = Path(output_dir) / Path(input_filepath).name

    # Читаем и копируем загруженный файл в temp_path
    with open(input_filepath, 'rb') as f:
        uploaded_file = f.read()

    with open(temp_path, 'wb') as tfile:
        tfile.write(uploaded_file)
        video_path = str(temp_path)

    # Загружаем видео
    clip = moviepy.VideoFileClip(video_path)

    reduced_video_path = str(
        Path(output_dir) / (Path(temp_path).stem + "_reduced" + Path(temp_path).suffix)
    )

    # Создаем новый клип с новыми fps
    new_clip = clip.set_fps(fps)
    new_clip.write_videofile(reduced_video_path)

    click.echo(reduced_video_path)


if __name__ == "__main__":
    upload_and_preprocess_video()

# @click.command()
# @click.argument("input_filepath", type=click.Path(exists=True))
# @click.argument("output_dir", type=click.Path())
# @click.option("--fps", default=10, help="FPS для выходного видео.")
# def upload_and_preprocess_video(input_filepath, output_dir, fps):
#     # Ensure the output directory exists
#     os.makedirs(output_dir, exist_ok=True)

#     # Сохраняем временный файл в output_dir напрямую
#     temp_path = Path(output_dir) / Path(input_filepath).name

#     # Читаем и копируем загруженный файл в temp_path
#     with open(input_filepath, 'rb') as f:
#         uploaded_file = f.read()

#     with open(temp_path, 'wb') as tfile:
#         tfile.write(uploaded_file)
#         video_path = str(temp_path)

#     # Загружаем видео
#     clip = moviepy.VideoFileClip(video_path)

#     # Новый путь для видео с суффиксом _reduced
#     reduced_video_path = str(
#         Path(output_dir) / (Path(temp_path).stem + "_reduced" + Path(temp_path).suffix)
#     )

#     # Создаем новый клип с новыми fps
#     new_clip = clip.set_fps(fps)

#     # Включаем логирование покадровой обработки
#     new_clip.write_videofile(reduced_video_path, logger='bar')

#     click.echo(reduced_video_path)

# if __name__ == "__main__":
#     upload_and_preprocess_video()
