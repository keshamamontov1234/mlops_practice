import click
from ultralytics import YOLO


@click.command()
@click.argument("model_path", type=click.Path(exists=True))
def load_model(model_path):
    model = YOLO(model_path)
    click.echo("Model loaded")


if __name__ == "__main__":
    load_model()
