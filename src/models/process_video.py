import click
from pathlib import Path
from ultralytics import YOLO
import os
import moviepy.editor as mp

@click.command()
@click.argument("model_path", type=click.Path(exists=True))
@click.argument("video_path", type=click.Path(exists=True))
@click.argument("output_dir", type=click.Path())
@click.option("--conf", default=0.3, help="Confidence threshold for predictions")
def process_video(model_path, video_path, output_dir, conf):
    os.makedirs(output_dir, exist_ok=True)

    if not os.path.isfile(video_path):
        raise FileNotFoundError(f"Input video file {video_path} does not exist.")

    model = YOLO(model_path)
    proc_filename = "processed_video"
    results = model.predict(
        source=video_path,
        conf=conf,
        save=True,
        project=output_dir,
        name=proc_filename
    )
    
    # Определение путей для исходного и окончательного выходного файлов
    intermediate_output_path = Path(output_dir) / proc_filename / f"{Path(video_path).stem}.avi"
    final_output_path = Path(output_dir) / "processed_video.mp4"
    
    # Конвертация avi в mp4
    if intermediate_output_path.exists():
        clip = mp.VideoFileClip(str(intermediate_output_path))
        clip.write_videofile(str(final_output_path), codec='libx264')
        
        # Удаление временного .avi файла
        intermediate_output_path.unlink()
        
        print(f"Results saved to {final_output_path}")
    else:
        raise FileNotFoundError(f"Intermediate output file {intermediate_output_path} does not exist.")

if __name__ == "__main__":
    process_video()
